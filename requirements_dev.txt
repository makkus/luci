# dev dependencies

pip==10.0.1
bumpversion==0.5.3
wheel==0.31.1
watchdog==0.8.3
ipython # pyup: ignore
six>=1.11.0

# docs

Sphinx==1.7.5
sphinx-autobuild==0.7.1
docutils==0.14
sphinxcontrib-napoleon==0.6.1
sphinx_redactor_theme==0.0.1
tornado<=5.0    # pyup: ignore

# pypi

twine==1.11.0

# testing & ci
pytest==3.6.2
pytest-runner==4.2
flake8==3.5.0
tox==3.0.0
coverage==4.5.1
coveralls==1.3.0
pytest-cov==2.5.1
pytest-pep8==1.0.6
# other

Click>=7.0
sphinx-click==1.3.0
jinja2>=2.10
stevedore>=1.28.0
future>=0.16.0

# dev dependencies
git+https://gitlab.com/frkl/frutils.git
git+https://gitlab.com/frkl/frkl.git
