#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `lucify` package."""

import pytest

from luci.lucifiers import *
from luci.readers import *

from frutils import special_dict_to_dict

CWD = os.path.dirname(os.path.realpath(__file__))
PATH_L = os.path.join(CWD, "lucifier_test")

yaml_reader = LucifyYamlDictionaryDictletReader()

DICTLET_1 = {"path": os.path.join(PATH_L, "minimal_1.yml"), "type": "file"}
DICTLET_2 = {"path": os.path.join(PATH_L, "minimal_2.yml"), "type": "file"}

LUC_DATA = [
    ("minimal", DICTLET_1, {"minimal": {"a": 1, "b": 2, "c": {"d": 4}}}),
    ("minimal", DICTLET_2, {"minimal": {"a": 1, "b": 2, "c": {"d": 4}}}),
]


@pytest.mark.parametrize("name, dictlet_details, expected", LUC_DATA)
def test_metadata_lucifier(name, dictlet_details, expected):

    md = MetadataLucifier()
    md.overlay_dictlet(name, copy.deepcopy(dictlet_details), add_dictlet=True)

    result = special_dict_to_dict(md.process())

    assert result == expected


LUC_DATA_2 = [
    ("minimal", DICTLET_1, {"minimal": {"a": 1, "b": 2, "c": {"d": 4}}}),
    ("minimal", DICTLET_2, {"minimal": {"a": 1, "b": 2, "c": {"d": 4}}}),
]

# @pytest.mark.parametrize("name, dictlet_details, expected", LUC_DATA_2)
# def test_debug_lucifier(name, dictlet_details, expected):
#
#     dd = DebugLucifier()
#     dd.overlay_dictlet(name, copy.deepcopy(dictlet_details), add_dictlet=True)
#
#     result = special_dict_to_dict(dd.process())
#
#     assert result == expected
