import pytest
import os

from luci.lucifiers import *
from luci.finders import *
from luci.readers import *

from frutils import special_dict_to_dict

CWD = os.path.dirname(os.path.realpath(__file__))
PATH_RD = os.path.join(CWD, "reader_test")
READER_LOAD_DATA = [
    ("yaml", {}, {"path": os.path.join(PATH_RD, "one", "one")}, {}, {"key1": "value1"}),
    (
        "yaml",
        {},
        {"path": os.path.join(PATH_RD, "one", "one")},
        {"key1": "XX"},
        {"key1": "value1"},
    ),
    ("class", {}, {"class": MetadataLucifier}, {}, {}),
    ("class", {}, {"class": MetadataLucifier}, {"k1": "v1"}, {"k1": "v1"}),
    (
        "yaml-folder",
        {},
        {"path": os.path.join(PATH_RD, "yaml-folder")},
        {},
        {"one": {"one_one": 11, "one_two": 12}, "two": {"two_one": 21, "two_two": 22}},
    ),
]


@pytest.mark.parametrize(
    "reader_name, reader_config, dictlet_details, current_md, expected",
    READER_LOAD_DATA,
)
def test_finder_loading(
    reader_name, reader_config, dictlet_details, current_md, expected
):

    reader = create_dictlet_reader(reader_name, reader_config)

    result = reader.read_dictlet(dictlet_details, current_md)
    print(CWD)
    assert special_dict_to_dict(result) == expected
