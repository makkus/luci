import pytest
import os

from luci.lucifiers import *
from luci.finders import *

CWD = os.path.dirname(os.path.realpath(__file__))

FINDER_LOAD_DATA = [
    ("path", {"paths": [os.path.join(CWD, "finder_test", "empty")]}, []),
    ("path", {"paths": [os.path.join(CWD, "finder_test", "one")]}, ["one", "two"]),
]


@pytest.mark.parametrize("finder_name, finder_config, expected", FINDER_LOAD_DATA)
def test_finder_loading(finder_name, finder_config, expected):

    finder = create_dictlet_finder(finder_name, finder_config)

    result = finder.get_all_dictlet_names()
    print(CWD)
    assert sorted(result) == sorted(expected)


def test_finder_loading_error():

    with pytest.raises(Exception):
        create_dictlet_finder("wrong", {})


def test_lucifier_finder():

    lf = LucifierFinder()

    dictlet_names = lf.get_all_dictlet_names()

    assert "debug" in dictlet_names
    assert "download" in dictlet_names
    assert "metadata" in dictlet_names
    assert "template" in dictlet_names
    assert "cat" in dictlet_names

    # dictlet = lf.get_dictlet("debug")
    # assert dictlet == {"class": DebugLucifier, "name": "debug"}


def test_folder_finder():

    ff = FolderFinder()
    path = os.path.join(CWD, "finder_test", "one")
    result = ff.get_dictlet(path)

    assert result == {"path": path, "type": "folder"}
