import os
from pprint import pprint

from luci.lucifiers import *
from luci.finders import *
from luci.readers import *

CWD = os.path.dirname(os.path.realpath(__file__))
PATH_FRD = os.path.join(CWD, "repo_examples")


def test_reader_only_files():

    path = os.path.join(PATH_FRD, "only_files")

    fr = LuItemFolderReader(use_files=True, use_metadata_files=False)
    result = fr.read_dictlet({"path": path, "type": "folder"}, {})

    pprint(result)

    assert len(result) == 10
    for pkg_name, details in result.items():

        assert pkg_name == details["luitem"]["name"]
        # assert len(details["urls"]) == 1
        # assert details["urls"][0]["file_name"] == pkg_name


def test_reader_metadata_only():

    path = os.path.join(PATH_FRD, "only_metadata")

    fr = LuItemFolderReader(use_files=False, use_metadata_files=True)
    result = fr.read_dictlet({"path": path, "type": "folder"}, {})

    pprint(result)

    assert len(result) == 8
    for pkg_name, details in result.items():

        assert pkg_name == details["luitem"]["name"]
        # assert len(details["urls"]) == 1
        # assert details["urls"][0]["file_name"] == "{}.pkg".format(pkg_name)
