#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `luci` package."""

import pytest
from click.testing import CliRunner

from luci import cli
from luci import plugins


# ---------------------------------------
# helper methods
def get_all_lucifier_names():

    return plugins.get_plugin_names()


# ---------------------------------------
# tests


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # from bs4 import BeautifulSoup
    # assert 'GitHub' in BeautifulSoup(response.content).title.string


def test_command_line_interface_help():

    """Test the CLI."""
    runner = CliRunner()
    help_result = runner.invoke(cli.cli)
    assert help_result.exit_code == 0
    assert "Show this message and exit." in help_result.output

    help_result = runner.invoke(cli.cli, ["--help"])
    assert help_result.exit_code == 0
    assert "Show this message and exit." in help_result.output


def test_command_line_lucifiers_interface_help():

    runner = CliRunner()
    for lucifier_name in get_all_lucifier_names():

        help_result = runner.invoke(cli.cli, [lucifier_name, "--help"])
        assert help_result.exit_code == 0
        assert "Show this message and exit." in help_result.output
