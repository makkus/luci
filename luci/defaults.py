# -*- coding: utf-8 -*-

import os

# DEFAULTS

LUCI_CONFIG_DIR = os.path.expanduser("~/.luci")
LUCI_CONFIG_FILE = os.path.join(LUCI_CONFIG_DIR, "defaults.yml")

LUCI_EPILOG_TEXT = "luci is free and open source software, for more information visit: https://github.com/makkus/luci"
DEFAULT_LUCI_CONFIG = {}


DEFAULT_DICTLET_READER = "yaml"
DICTLET_READER_MARKER_STRING = "lucify"
DICTLET_START_MARKER_STRING = "start"
DICTLET_STOP_MARKER_STRING = "stop"

PATH_FINDER_MAX_FOLDER_DEFAULT = 2

# KEYS (under KEY_LUCI_NAME):
ALL_LUCIFIERS_KEY = "all"
LUCIFIER_CLI_DEFAULT_DICTLET_VARS = "{}.cli.default_dictlet_vars"
LUCIFIER_DEFAULT_READER = "{}.lucifiers.{}.reader"  # needs keys 'name' and 'config'
LUCIFIER_DEFAULT_FINDER = "{}.lucifiers.{}.finder"  # needs keys 'name' and 'config'
DEFAULT_FINDER_CONFIG = {"name": "path", "config": {"max_folders": 1}}

DEFAULT_READER_CONFIG = {"name": "yaml"}

DEFAULT_DICTLET_READER = "yaml"
DICTLET_READER_MARKER_STRING = "lucify"
DICTLET_START_MARKER_STRING = "start"
DICTLET_STOP_MARKER_STRING = "stop"

PATH_FINDER_MAX_FOLDER_DEFAULT = 2

# KEYS (under KEY_LUCI_NAME):
ALL_LUCIFIERS_KEY = "all"
LUCIFIER_CLI_DEFAULT_DICTLET_VARS = "{}.cli.default_dictlet_vars"
LUCIFIER_DEFAULT_READER = "{}.lucifiers.{}.reader"  # needs keys 'name' and 'config'
LUCIFIER_DEFAULT_FINDER = "{}.lucifiers.{}.finder"  # needs keys 'name' and 'config'
DEFAULT_FINDER_CONFIG = {"name": "path", "config": {"max_folders": 1}}

DEFAULT_READER_CONFIG = {"name": "yaml"}
