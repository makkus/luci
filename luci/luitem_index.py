# -*- coding: utf-8 -*-
"""Main module."""
import abc
import copy
import logging
import os
import sys
from collections import OrderedDict

from cerberus import Validator
from jinja2 import Environment
# import yaml
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap
from six import string_types
from stevedore import driver

from frkl import dict_from_url, download_cached_file
from frutils import is_url_or_abbrev
from luci.finders import FolderOrFileFinder
from luci.lucify import Lucifier
from luci.luitem import create_item
from luci.readers import LuItemFolderReader
from .exceptions import DictletParseException

log = logging.getLogger("lucify")

DEFAULT_PKG_TYPE = "default"
ENV = Environment()

INDEX_DESC_SCHEMA = {
    "url": {"type": "string"},
    "base_url": {"type": "string"},
    "type": {"type": "string"},
    "init_params": {"type": "dict", "allow_unknown": True},
}
INDEX_DESC_VALIDATOR = Validator(INDEX_DESC_SCHEMA)

LUPKG_CACHE_BASE = os.path.expanduser("~/.local/share/lupkg/cache")

INDEX_DESC_SCHEMA = {
    "alias": {"type": "string"},
    "url": {"type": "string"},
    "item_type": {"type": "string"},
    "base_url": {"type": "string"},
    "type": {"type": "string"},
    "init_params": {"type": "dict", "allow_unknown": True},
}
INDEX_DESC_VALIDATOR = Validator(INDEX_DESC_SCHEMA)


# =============================================================
# global index management


def create_index_desc(url_orig, item_type="default", index_type=None):

    remote = is_url_or_abbrev(url_orig)
    if remote:
        if url_orig.endswith(".git"):
            raise Exception("Remote index folders not supported yet")
        is_file = True
        url = download_cached_file(url_orig)
    else:
        real_path = os.path.realpath(os.path.expanduser(url_orig))
        if not os.path.exists(real_path):
            raise Exception("Index url does not exist: {}".format(url_orig))
        if os.path.isfile(real_path):
            is_file = True
        else:
            is_file = False
        url = url_orig

    if remote:
        if index_type is None:
            index_type = "file"
        base_url = os.path.dirname(url)
    else:
        if is_file:
            if index_type is None:
                index_type = "file"
            base_url = os.path.dirname(url)
        else:
            if index_type is None:
                index_type = "folder"
            base_url = url

    desc = {}
    desc["alias"] = url_orig
    desc["url"] = url
    desc["item_type"] = item_type
    desc["base_url"] = base_url
    desc["type"] = index_type
    desc["init_params"] = {}

    return desc


def create_index(index_desc_dict):

    log.debug("Creating index for description: {}".format(index_desc_dict))
    # print("Creating index for description: {}".format(index_desc_dict))
    valid = INDEX_DESC_VALIDATOR(index_desc_dict)
    if not valid:
        errors = copy.deepcopy(INDEX_DESC_VALIDATOR.errors)
        raise Exception(
            "Can't parse index description '{}': {}".format(index_desc_dict, errors)
        )

    index_type = index_desc_dict["type"]
    item_type = index_desc_dict["item_type"]
    url = index_desc_dict["url"]
    base_url = index_desc_dict["base_url"]
    init_params = copy.deepcopy(index_desc_dict["init_params"])

    init_params["url"] = url
    init_params["item_type"] = item_type
    init_params["pkg_base_url"] = base_url
    init_params["alias"] = index_desc_dict.get("alias", None)

    if index_type == "multi":

        raise Exception("Dynamically creating multi-indexes not supported yet")

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(
        logging.Formatter("dictlet_finder plugin error -> %(message)s")
    )
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    log.debug("Loading dictlet_finder...")

    mgr = driver.DriverManager(
        namespace="luci.indexes",
        name=index_type,
        invoke_on_load=True,
        invoke_kwds=init_params,
    )

    log.debug(
        "Registered lupkg index: {}".format(
            ", ".join(ext.name for ext in mgr.extensions)
        )
    )

    return mgr.driver


DEFAULT_INDEX_NAME = "default"


class LuItemIndex(object):
    """Base class to hold information about a package repository.

    This can be extended by classes that wish to implement a specific
    way of storing and querying that data (e.g. using a database etc.).
    """

    # @classmethod
    # def create(cls, *urls):
    #
    #     descs = expand_index_url(urls)
    #     if len(descs) == 0:
    #         raise Exception("Could not find any indexes for url(s): {}".format(urls))
    #     elif len(descs) == 1:
    #         index = create_index(descs[0])
    #     else:
    #         indexes = []
    #         for d in descs:
    #             i = create_index(d)
    #             indexes.append(i)
    #
    #         index = LuItemMultiIndex(None, None, indexes)
    #
    #     return index

    def __init__(
        self,
        url=None,
        item_type="default",
        alias=None,
        pkg_base_url=None,
        ignore_invalid_pkg_metadata=False,
    ):

        log.debug("Creating index for url '{}' and type '{}'...".format(url, item_type))
        if alias is None:
            alias = url
        self.name = alias
        self.url = url
        self.item_type = item_type
        if pkg_base_url is None:
            pkg_base_url = url
        self.pkg_base_url = pkg_base_url
        self.cached_pkgs = {}
        self.ignore_invalid_pkg_metadata = ignore_invalid_pkg_metadata

    def get_name(self):
        return self.name

    def update(self):

        self.cached_pkgs = {}
        self.update_index()

    @abc.abstractmethod
    def update_index(self):
        """Updates the index."""

        pass

    # @abc.abstractmethod
    # def get_package_types(self):
    #     """Returns a list of package types in this repository.
    #
    #     Returns:
    #         list: the list of package types
    #     """
    #     pass

    @abc.abstractmethod
    def get_available_packages(self):
        """Returns all available package names in no particular order.

        Returns:
          list: a list of package names
        """

        pass

    def get_pkg_names(self):
        """Returns a sorted list of available package names.

        Returns:
          list: a list of package names
        """

        if self.get_available_packages() is None:
            return []

        return sorted(self.get_available_packages())

    def get_all_metadata(self):

        result = {}
        for pn in self.get_pkg_names():
            pkg_md = self.get_pkg_metadata(pn)
            result[pn] = pkg_md

        return result

    def save_index_file(self, path):

        yaml = YAML()
        yaml.default_flow_style = False
        with open(path, "w") as fi:
            yaml.dump(self.get_all_metadata(), fi)

    @abc.abstractmethod
    def get_pkg_metadata(self, name):
        """Returns the metadata of a package (in LuPKG format).

        Args:
          name (str): the package name

        Returns:
          dict: the metadata
        """

        pass

    # def get_pkgs_with_file(self, file):
    #     """Returns a list of package names that contain a file with the specified name."""
    #
    #     result = set()
    #     for pn in self.get_pkg_names():
    #
    #         pkg = self.get_pkg(pn)
    #         if pkg.has_file(file):
    #             result.add(pn)
    #
    #     return list(result)

    def get_parent_index_for_pkg(self, name):

        return self

    def get_pkg(self, name):
        """Returns the :class:`~LuPKG` object for the specified package name.

        Return:
          LuPKG: the package details, or none if package doesn't exist
        """
        if name not in self.cached_pkgs.keys():
            pkg_details = self.get_pkg_metadata(name)
            if not pkg_details:
                self.cached_pkgs[name] = None
            else:
                try:
                    parent_index = self.get_parent_index_for_pkg(name)
                    pkg = create_item(
                        self.item_type,
                        pkg_details,
                        base_url=self.pkg_base_url,
                        index=self,
                        parent_index=parent_index,
                    )
                    self.cached_pkgs[name] = pkg
                except (Exception) as e:
                    log.debug(e, exc_info=1)
                    if self.ignore_invalid_pkg_metadata:
                        log.debug("Can't parse item '{}', ignoring it...".format(name))
                        self.cached_pkgs[name] = "__FAILED_MARKER__"
                        return None
                    else:
                        raise DictletParseException(
                            "Can't parse item '{}': {}".format(name, e), name
                        )
                # pkg = LuPKG(pkg_details, base_url=self.pkg_base_url)

        result = self.cached_pkgs[name]
        if result == "__FAILED_MARKER__":
            return None
        return result

    # def get_all_urls(self, properties=None):
    #     """Returns a map of all package names with their corresponding url for the latest version.
    #
    #     Args:
    #       properties (dict): optional properties to be used in the package selection
    #
    #     Returns:
    #       OrderedDict: a dict with the package name as key, and the url as value
    #     """
    #
    #     result = OrderedDict()
    #     for pn in self.get_pkg_names():
    #         pkg = self.get_pkg(pn)
    #         # pkg = self.repos.pkg_descs.get(pn, None)
    #         try:
    #             url = pkg.get_url_details(properties=properties)
    #             result[pn] = url
    #         except (LupkgMetadataException) as e:
    #             log.error(e)
    #
    #     return result


class LuItemFileIndex(LuItemIndex):
    """:class:`LuPKGS` implementation that reads one or multiple yaml files to get package metadata.
    """

    def __init__(
        self, url=None, item_type="default", alias=None, pkg_base_url=None, **kwargs
    ):

        if not isinstance(url, string_types):
            raise Exception(
                "Index file needs to be of type string, instead it is '{}'".format(
                    type(url)
                )
            )

        if pkg_base_url is None:
            pkg_base_url = os.path.dirname(url)

        if alias is None:
            alias = url

        super(LuItemFileIndex, self).__init__(
            url=url,
            item_type=item_type,
            alias=alias,
            pkg_base_url=pkg_base_url,
            **kwargs
        )
        self.get_index()

    def update_index(self):

        return self.get_index(update=True)

    def get_index(self, update=False):

        log.debug("Updating index: {}".format(self.url))
        # print("Updating index: {}".format(self.url))

        self.metadata = dict_from_url(
            self.url, update=update, cache_base=LUPKG_CACHE_BASE
        )

    def get_pkg_metadata(self, name):

        return self.metadata.get(name, None)

    def get_available_packages(self):

        return self.metadata.keys()


class LuItemMultiIndex(LuItemIndex):
    """:class:`LuPKGS` implementation to hold multiple, different LuPKGS objects

    Args:
        name (str): the name of this index
        lupkgs (list): a list of LuItemIndex objects
        **kwargs (dict): additional init values

    """

    def __init__(
        self,
        url=None,
        item_type="default",
        alias=None,
        pkg_base_url=None,
        indexes=None,
        **kwargs
    ):

        # url/base_url is not used in this one
        if indexes is None:
            raise Exception("No child indexes specified.")

        if not isinstance(alias, string_types):
            raise Exception("No alias provided.")

        super(LuItemMultiIndex, self).__init__(
            url=url,
            item_type=item_type,
            alias=alias,
            pkg_base_url=pkg_base_url,
            **kwargs
        )

        self.packages = OrderedDict()
        self.indexes = indexes

        self.update_index()
        # for index in self.indexes:
        #     for pkg_name in index.get_pkg_names():
        #         if pkg_name not in self.packages.keys():
        #             self.packages[pkg_name] = index

    def update_index(self):

        self.packages = OrderedDict()

        for i in self.indexes:

            i.update_index()
            duplicates = []
            for pkg_name in i.get_pkg_names():
                if pkg_name not in self.packages.keys():
                    self.packages[pkg_name] = i
                else:
                    duplicates.append(pkg_name)
            if duplicates:
                msg = "Ignored duplicates from index '{}':".format(i.name)
                for dup in duplicates:
                    msg += "\n  - {}".format(dup)
                log.warn(msg)

    def get_pkg_metadata(self, name):

        index = self.packages.get(name, None)
        if index is None:
            return None
        md = index.get_pkg_metadata(name)
        return md

    def get_parent_index_for_pkg(self, name):

        index = self.packages.get(name, None)
        # if isinstance(index, LuItemMultiIndex):
        #     return index.get_index_for_pkg(name)
        # else:
        return index

    def get_available_packages(self):

        return self.packages.keys()


class LupkgFolderLucifier(Lucifier):
    def __init__(self, reader_params=None, **kwargs):

        super(LupkgFolderLucifier, self).__init__("repo", **kwargs)

        if reader_params is None:
            reader_params = {}
        self.reader = LuItemFolderReader(**reader_params)
        self.finder = FolderOrFileFinder()

        self.pkg_descs = CommentedMap()

    def get_default_dictlet_reader(self):

        return self.reader

    def get_default_dictlet_finder(self):

        return self.finder

    def process_dictlet(self, metadata, dictlet_details=None):

        for pkg_name, details in metadata.items():

            if pkg_name in self.pkg_descs.keys():
                log.warning(
                    "Duplicate description for package '{}', overwriting existing one...".format(
                        pkg_name
                    )
                )

            self.pkg_descs[pkg_name] = details


class LuItemFolderIndex(LuItemIndex):
    """:class:`~LuPKGS` implementation that stores metadata about packages
    in a folder structure.

    Internally this uses the `luci <https://github.com/makkus/luci>`_
    python library to read the metadata from the folder. This happens
    in the :class:`~LupkgMetadataFolderLucifier` class to hold the relevant metadata
    derived from the folder.

    """

    def __init__(
        self,
        url=None,
        item_type="default",
        alias=None,
        pkg_base_url=None,
        reader_params=None,
        **kwargs
    ):

        if pkg_base_url is None:
            pkg_base_url = url
        if alias is None:
            alias = url
        if reader_params is None:
            reader_params = {}
        super(LuItemFolderIndex, self).__init__(
            url=url,
            item_type=item_type,
            alias=alias,
            pkg_base_url=pkg_base_url,
            **kwargs
        )
        self.reader_params = reader_params
        self.update_index()

    def update_index(self):

        self.repos = LupkgFolderLucifier(reader_params=self.reader_params)
        self.repos.overlay_dictlet(self.url, add_dictlet=True)
        self.repos.process()

    def get_pkg_metadata(self, name):

        return self.repos.pkg_descs.get(name, None)

    def get_available_packages(self):

        return self.repos.pkg_descs.keys()
