# -*- coding: utf-8 -*-
"""Main module."""

import copy
import logging
import os
import sys

from stevedore import driver

from frutils.doc import Doc
from .exceptions import LuItemSchemaException

log = logging.getLogger("lucify")


def create_item(item_type, metadata, base_url, index=None, parent_index=None):

    metadata = copy.deepcopy(metadata)

    log2 = logging.getLogger("stevedore")
    out_hdlr = logging.StreamHandler(sys.stdout)
    out_hdlr.setFormatter(logging.Formatter("item plugin error -> %(message)s"))
    out_hdlr.setLevel(logging.DEBUG)
    log2.addHandler(out_hdlr)
    log2.setLevel(logging.INFO)

    # log.debug("Creating luitem: {}".format(metadata))
    # log.debug("Loading index item of type: {}".format(item_type))

    mgr = driver.DriverManager(
        namespace="luci.index_item_types",
        name=item_type,
        invoke_on_load=True,
        invoke_args=(metadata,),
        invoke_kwds={
            "base_url": base_url,
            "index": index,
            "parent_index": parent_index,
        },
    )

    # log.debug("Created item: {}".format(", ".join(ext.name for ext in mgr.extensions)))

    return mgr.driver


class LuItem(object):
    """Base class to hold an index item."""

    def __init__(self, metadata, base_url=None, index=None, parent_index=None):

        if base_url is None:
            base_url = os.getcwd()
        self.base_url = base_url
        self.index = index
        self.parent_index = parent_index
        self.metadata_raw = metadata
        self.metadata = self.process_metadata(metadata)

        # log.debug("Processed metadata: {}".format(self.metadata))

        if self.get_metadata_validator() is not None:
            log.debug("Validating metadata...")
            valid = self.get_metadata_validator()(self.metadata)
            if not valid:
                errors = copy.deepcopy(self.get_metadata_validator().errors)
                raise LuItemSchemaException(
                    "Can't validate metadata: {}".format(errors),
                    item=self.metadata,
                    validation_errors=copy.deepcopy(errors),
                )

        for key, data in self.metadata.items():
            if key == "doc":
                setattr(self, "doc_raw", data)
                data = Doc(data)
            setattr(self, key, data)

    def get_metadata_validator(self):

        return None

    def process_metadata(self, metadata):

        return metadata
