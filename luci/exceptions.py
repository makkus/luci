# -*- coding: utf-8 -*-


class NoSuchDictletException(Exception):
    def __init__(self, dictlet_name):

        super(NoSuchDictletException, self).__init__(
            "No such dictlet: {}".format(dictlet_name)
        )
        self.dictlet = dictlet_name


class DictletParseException(Exception):
    def __init__(self, message, dictlet_name):

        super(DictletParseException, self).__init__(
            "Error parsing dictlet '{}': {}".format(dictlet_name, message)
        )
        self.dictlet_name = dictlet_name


class LuItemSchemaException(Exception):
    def __init__(self, message, item=None, validation_errors=None):

        super(LuItemSchemaException, self).__init__(message)

        self.message = "{}: {}".format(message, validation_errors)
        self.item = item
        self.validation_errors = validation_errors
