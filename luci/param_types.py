# -*- coding: utf-8 -*-

import os

import click

from frkl import (
    Frkl,
    UrlAbbrevProcessor,
    EnsureUrlProcessor,
    EnsurePythonObjectProcessor,
    LoadMoreConfigsProcessor,
)
from frutils import dict_merge


# ===================================================
# ParamTypes


class VarsFileType(click.ParamType):
    name = "vars_type"

    def convert(self, value, param, ctx):

        chain = [
            UrlAbbrevProcessor(),
            EnsureUrlProcessor(),
            EnsurePythonObjectProcessor(),
            LoadMoreConfigsProcessor(),
        ]

        try:
            if not isinstance(value, (list, tuple)):
                value = [value]

            frkl_obj = Frkl(value, chain)
            result = frkl_obj.process()

            if isinstance(result[0], (list, tuple)):

                result_dict = {}
                for item in result[0]:
                    dict_merge(result_dict, item, copy_dct=False)

                return result_dict
            else:
                return result[0]

        except Exception as e:
            self.fail("Can't read vars '{}': {}".format(value, str(e)))

    def complete(self, ctx, incomplete):

        if incomplete.startswith("/"):
            path_orig = incomplete
        else:
            path_orig = os.path.join(os.getcwd(), incomplete)

        path, file_prefix = path_orig.rsplit("/", 1)

        result = []
        if not path:
            path = "/"

        for filename in os.listdir(path):

            if not file_prefix or filename.startswith(file_prefix):

                file_path = os.path.join(path, filename)
                if os.path.isdir(file_path):
                    filename += "/"

                if incomplete.startswith("/"):
                    result.append(os.path.join(path, filename))
                else:
                    result.append(filename)

        return result
